# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

# Exemple d'utilisation du package dcelectricity

import dcelectricity.dc_fr as dc

if dc.__version__ < (0, 2, 4):
    raise Exception("La version du package est trop ancienne")

print("""Schéma électrique :

          U1
       <------
    --->--R1-------------
 ^     I1         |      |
 |            I2  v      v  I3   ^
 |                |      |       |
E|                R2     R3      | U2
 |                |      |       |
 |                |      |
    ---------------------
""")

"""
Données :
E = 12 V
R1 = 1 kΩ
R2 = 2.7 kΩ
R3 = 1.8 kΩ
"""

loi = dc.Loi()

# définitions
E = dc.Tension(12)
E.Info("Propriétés de E :")

R1 = dc.Resistance(1, 'k')
R1.Info("Propriétés de R1 :")

R2 = dc.Resistance(2.7, 'k')
R2.Info("Propriétés de R2 :")

R3 = dc.Resistance(1.8, 'k')
R3.Info("Propriétés de R3 :")

# résistance équivalente
Req = loi.Rserie(R1, loi.Rparallele(R2, R3))

# loi d'Ohm
I1 = loi.Ohm(v=E, r=Req)

# loi d'Ohm
U1 = loi.Ohm(i=I1, r=R1)

# loi des branches
U2 = loi.Branche("+-", E, U1)

# loi d'Ohm
I2 = loi.Ohm(r=R2, v=U2)

# loi des nœuds
I3 = loi.Noeud("+-", I1, I2)

# autre méthode pour trouver U2
# formule du diviseur de tension
U2bis = loi.DiviseurTension(vtotal=E, r=loi.Rparallele(R2, R3), r2=R1)

# autre méthode pour trouver I2
# formule du diviseur de courant
I2bis = loi.DiviseurCourant(itotal=I1, r=R2, r2=R3)

# encore une autre méthode pour trouver U2
# Théorème de Millman
masse = dc.Tension(0)
U2ter = loi.Millman(v_r=[(E, R1), (masse, R2), (masse, R3)])

# bilan de puissances
puissanceE = loi.Puissance(v=E, i=I1)
puissanceE.Info("Générateur E :")

P1 = loi.Joule(r=R1, i=I1)
P1.Info("Résistance R1 :")
P2 = loi.Joule(r=R2, i=I2)
P2.Info("Résistance R2 :")
P3 = loi.Joule(r=R3, i=I3)
P3.Info("Résistance R3 :")
(P1+P2+P3).Info("Puissance totale consommée par les résistances :")
