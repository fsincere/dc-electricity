# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

# Exemple d'utilisation du package dcelectricity

import dcelectricity.dc_fr as dc

if dc.__version__ < (0, 2, 4):
    raise Exception("La version du package est trop ancienne")

print("""Schéma électrique :


          U1
       <------
    --->--R1-------------
 ^     I1         |      |
 |            I2  v      v  I3   ^
 |                |      |       |
E|                R2     R3      | U2
 |                |      |       |
 |                |      |
    ---------------------

E = 12 V
R1 = 1 kΩ
R2 = 2.7 kΩ

On cherche la valeur de R3 qui donne U2 = 6.00 V
""")


def f(x):
    """
Ici, fonction au sens mathématique : U2 = f(R3)
x --> R3 (float)
retourne la valeur de la tension U2 (float)
"""

    R3 = dc.Resistance(x)

    # Diviseur de tension
    U2 = ((R2//R3)/(R1 + (R2//R3)))*E

    return U2()  # ou U2.Valeur()


def solution(xmin, xmax, y0, xprecision):
    """
Recherche d'une solution numérique x0 de l'équation f(x) = y0 \
par la méthode dichotomique.
Intervalle de recherche : xmin, xmax
Valeur cible : y0
Précision (absolue) sur la solution x0 : xprecision
Remarque : le nombre d'itération dépend de la précision demandée.
"""

    y1 = f(xmin)-y0
    y2 = f(xmax)-y0

    if y1 == 0.:
        # solution exacte
        print("SOLUTION EXACTE")
        return xmin
    if y2 == 0.:
        # solution exacte
        print("SOLUTION EXACTE")
        return xmax

    if y1*y2 > 0:
        raise Exception("Impossible de trouver une solution \
                        dans cet intervalle")

    xprecision = abs(xprecision)

    nb_iteration = 0
    while abs(xmax-xmin) > xprecision:
        x0 = (xmin+xmax)*0.5
        y = f(x0)-y0

        nb_iteration += 1
        if y == 0.:
            # solution exacte
            print("{:=10} {:=20} {:=20}".format(nb_iteration, x0, y+y0))
            print("SOLUTION EXACTE")
            return x0
        elif y*y1 > 0.:
            xmin = x0
            y1 = y
        else:
            xmax = x0
            y2 = y

        print("{:=10} {:=20} {:=20}".format(nb_iteration, x0, y+y0))
    return x0

loi = dc.Loi()

# définitions
E = dc.Tension(12)
R1 = dc.Resistance(1, 'k')
R2 = dc.Resistance(2.7, 'k')

# intervalle de recherche
R3min = 0
R3max = 10000

# cible
U2 = 6.0

print("Itération  R3 (Ω)               U2 (V)")

resultat = solution(xmin=R3min, xmax=R3max, y0=U2, xprecision=0.1)
print("\nRésultat : {} Ω".format(resultat))
