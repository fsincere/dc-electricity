# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

# Exemple d'utilisation du package dcelectricity

import dcelectricity.dc_fr as dc

if dc.__version__ < (0, 2, 4):
    raise Exception("La version du package est trop ancienne")

print("""Schéma électrique :

          U1
       <------
    --->--R1-------------
 ^     I1         |      |
 |            I2  v      v  I3   ^
 |                |      |       |
E|                R2     R3      | U2
 |                |      |       |
 |                |      |
    ---------------------
""")

"""
Données :
E = 12 V
R1 = 1 kΩ
R2 = 2.7 kΩ
R3 = 1.8 kΩ
"""

loi = dc.Loi()

# définitions
E = dc.Tension(12)
E.Info("Propriétés de E :")

R1 = dc.Resistance(1, 'k')
R1.Info("Propriétés de R1 :")

R2 = dc.Resistance(2.7, 'k')
R2.Info("Propriétés de R2 :")

R3 = dc.Resistance(1.8, 'k')
R3.Info("Propriétés de R3 :")

# résistance équivalente
Req = R1 + (R2//R3)

# loi d'Ohm
I1 = E/Req

# loi d'Ohm
U1 = R1*I1

# loi des branches
U2 = E-U1

# loi d'Ohm
I2 = U2/R2

# loi des nœuds
I3 = I1-I2

# autre méthode pour trouver U2
# formule du diviseur de tension
U2bis = ((R2//R3)/Req)*E

# autre méthode pour trouver I2
# formule du diviseur de courant
G2 = 1/R2
G3 = 1/R3
I2bis = (G2/(G2+G3))*I1

# encore une autre méthode pour trouver U2
# Théorème de Millman
masse = dc.Tension(0)
U2ter = (E/R1 + masse/R2 + masse/R3)/(1/R1 + 1/R2 + 1/R3)

# bilan de puissances
puissanceE = E*I1
puissanceE.Info("Générateur E :")

P1 = R1*I1*I1
P1.Info("Résistance R1 :")
P2 = R2*I2*I2
P2.Info("Résistance R2 :")
P3 = R3*I3*I3
P3.Info("Résistance R3 :")
(P1+P2+P3).Info("Puissance totale consommée par les résistances :")
