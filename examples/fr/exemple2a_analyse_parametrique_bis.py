# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

# Exemple d'utilisation du package dcelectricity

import dcelectricity.dc_fr as dc

if dc.__version__ < (0, 2, 4):
    raise Exception("La version du package est trop ancienne")

print("""Schéma électrique :


          U1
       <------
    --->--R1-------------
 ^     I1         |      |
 |            I2  v      v  I3   ^
 |                |      |       |
E|                R2     R3      | U2
 |                |      |       |
 |                |      |
    ---------------------

E = 12 V
R1 = 1 kΩ
R2 = 2.7 kΩ

Etude de l'évolution de U2 en fonction de R3
""")

loi = dc.Loi()

# définitions
E = dc.Tension(12)
R1 = dc.Resistance(1, 'k')
R2 = dc.Resistance(2.7, 'k')

while True:
    # saisie R3
    try:
        valeur = float(input(("\nR3 = ? ")))
    except:
        break

    R3 = dc.Resistance(valeur)
    R3.Info("\nPropriétés de R3 :")

    # résistance équivalente
    Req = R1 + (R2//R3)

    # loi d'Ohm
    I1 = E/Req

    # loi d'Ohm
    U1 = R1*I1

    # loi des branches
    U2 = E-U1
    U2.Info("Propriétés de U2 :")
