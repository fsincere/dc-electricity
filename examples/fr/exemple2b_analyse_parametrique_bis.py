# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

# Exemple d'utilisation du package dcelectricity

import dcelectricity.dc_fr as dc

if dc.__version__ < (0, 2, 4):
    raise Exception("La version du package est trop ancienne")

try:
    import numpy as np
    import matplotlib.pyplot as plt
    import_matplotlib = True
except:
    import_matplotlib = False

print("""Schéma électrique :


          U1
       <------
    --->--R1-------------
 ^     I1         |      |
 |            I2  v      v  I3   ^
 |                |      |       |
E|                R2     R3      | U2
 |                |      |       |
 |                |      |
    ---------------------

E = 12 V
R1 = 1 kΩ
R2 = 2.7 kΩ

Etude de l'évolution de U2 en fonction de R3,
de la puissance consommée par R3 en fonction de R3,
et de U2 en fonction de I3
""")

loi = dc.Loi()

# définitions
E = dc.Tension(12)
R1 = dc.Resistance(1, 'k')
R2 = dc.Resistance(2.7, 'k')

# liste des valeurs de R3
# listeR3 = [0.1, 1, 10, 100, 1000, 10000, 100000]
# listeR3 = [10**i for i in range(-1, 6)]
listeR3 = [0.1+i*50 for i in range(400)]

listeI3 = []
listeU2 = []
listeP3 = []

print("R3 (Ω)               I3 (A)               U2 (V)               P3 (W)")

for valeur in listeR3:

    R3 = dc.Resistance(valeur)

    # résistance équivalente
    Req = R1 + (R2//R3)

    # loi d'Ohm
    I1 = E/Req

    # loi d'Ohm
    U1 = R1*I1

    # loi des branches
    U2 = E-U1

    listeU2.append(U2())  # U2() ou U2.Valeur()

    # loi d'Ohm
    I3 = U2/R3
    listeI3.append(I3())

    # puissance consommée par R3
    P3 = U2*I3
    listeP3.append(P3())

    print("{:=20} {:=20} {:=20} {:=20}".format(R3(), I3(), U2(), P3()))

if not import_matplotlib:
    exit("Pas de module matplotlib, pas de courbe !")

# courbes
fig, axe = plt.subplots(1, 1)

axe.grid(True)

axe.set_title("U2 en fonction de R3", fontsize=16)
axe.set_xlabel("R3 [Ω]")
axe.set_ylabel("U2 [V]")
axe.plot(listeR3, listeU2, 'b-', linewidth=2.0)

fig2, axe2 = plt.subplots(1, 1)

axe2.grid(True)

axe2.set_title("P3 en fonction de R3", fontsize=16)
axe2.set_xlabel("R3 [Ω]")
axe2.set_ylabel("P3 [W]")

axe2.plot(listeR3, listeP3, 'r-', linewidth=2.0,
          label="Pmax = {:f} W".format(max(listeP3)))
axe2.legend()

fig3, axe3 = plt.subplots(1, 1)

axe3.grid(True)

axe3.set_title("U2 en fonction de I3", fontsize=16)
axe3.set_xlabel("I3 [A]")
axe3.set_ylabel("U2 [V]")

# coefficients de la droite de régression linéaire
fit = np.polyfit(listeI3, listeU2, 1)

axe3.plot(listeI3, listeU2, 'g-', linewidth=2.0,
          label="U2 = {:+f}*I3 {:+f}".format(fit[0], fit[1]))
axe3.legend()

plt.show()
