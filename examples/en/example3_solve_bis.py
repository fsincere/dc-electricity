# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

# package dcelectricity example

import dcelectricity.dc_en as dc

print(dc.__version__)
if dc.__version__ < (0, 2, 4):
    raise Exception("package version is too old")

print("""DC Circuit diagram :

          V1
       <------
    --->--R1-------------
 ^     I1         |      |
 |            I2  v      v  I3   ^
 |                |      |       |
E|                R2     R3      | V2
 |                |      |       |
 |                |      |
    ---------------------

Datas :
E = 12 V
R1 = 1 kΩ
R2 = 2.7 kΩ

We are looking for the R3 resistance which gives V2 = 6.00 V
(dichotomy method).
""")


def f(x):
    """
mathematical function : V2 = f(R3)
x --> R3 resistance (float)
return V2 voltage (float)
"""

    R3 = dc.Resistor(x)

    # Voltage divider
    V2 = ((R2//R3)/(R1 + (R2//R3)))*E

    return V2()  # or V2.Value()


def solve(xmin, xmax, y0, xprecision):
    """Dichotomy method to numerically solve the equation f(x) = y0
Search interval : xmin, xmax
Target value : y0
x0 solution precision (absolute) : xprecision
"""

    y1 = f(xmin)-y0
    y2 = f(xmax)-y0

    if y1 == 0.:
        # exact solution
        print("EXACT SOLUTION")
        return xmin
    if y2 == 0.:
        # exact solution
        print("EXACT SOLUTION")
        return xmax

    if y1*y2 > 0:
        raise Exception("Can't find solution in this interval")

    xprecision = abs(xprecision)

    nb_iteration = 0
    while abs(xmax-xmin) > xprecision:
        x0 = (xmin+xmax)*0.5
        y = f(x0)-y0

        nb_iteration += 1
        if y == 0.:
            # exact solution
            print("{:=10} {:=20} {:=20}".format(nb_iteration, x0, y+y0))
            print("EXACT SOLUTION")
            return x0
        elif y*y1 > 0.:
            xmin = x0
            y1 = y
        else:
            xmax = x0
            y2 = y

        print("{:=10} {:=20} {:=20}".format(nb_iteration, x0, y+y0))
    return x0

# law = dc.Law()

# definitions
E = dc.Voltage(12)
R1 = dc.Resistor(1, 'k')
R2 = dc.Resistor(2.7, 'k')

# search interval
R3min = 0
R3max = 10000

# target
V2 = 6.0

print("Iteration  R3 (Ω)               V2 (V)")

res = solve(xmin=R3min, xmax=R3max, y0=V2, xprecision=0.1)
print("\nResult : {} Ω".format(res))
