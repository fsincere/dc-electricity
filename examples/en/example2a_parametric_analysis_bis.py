# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

# package dcelectricity example

import dcelectricity.dc_en as dc

print(dc.__version__)
if dc.__version__ < (0, 2, 4):
    raise Exception("package version is too old")

print("""DC Circuit diagram :

          V1
       <------
    --->--R1-------------
 ^     I1         |      |
 |            I2  v      v  I3   ^
 |                |      |       |
E|                R2     R3      | V2
 |                |      |       |
 |                |      |
    ---------------------

Datas :
E = 12 V
R1 = 1 kΩ
R2 = 2.7 kΩ

Study of the evolution of V2 as a function of R3
""")

# law = dc.Law()

# definitions
E = dc.Voltage(12)
R1 = dc.Resistor(1, 'k')
R2 = dc.Resistor(2.7, 'k')

while True:
    # R3 input
    try:
        value = float(input(("\nR3 = ? ")))
    except:
        break

    R3 = dc.Resistor(value)
    R3.Info("\nR3 properties :")

    # equivalent resistance
    Req = R1 + (R2//R3)

    # Ohm's law
    I1 = E/Req

    # Ohm's law
    V1 = R1*I1

    # Kirchhoff’s voltage law
    V2 = E-V1
    V2.Info("\nV2 properties :")
