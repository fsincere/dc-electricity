# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

# package dcelectricity example

import dcelectricity.dc_en as dc

print(dc.__version__)
if dc.__version__ < (0, 2, 4):
    raise Exception("package version is too old")

try:
    import numpy as np
    import matplotlib.pyplot as plt
    import_matplotlib = True
except:
    import_matplotlib = False

print("""DC Circuit diagram :

          V1
       <------
    --->--R1-------------
 ^     I1         |      |
 |            I2  v      v  I3   ^
 |                |      |       |
E|                R2     R3      | V2
 |                |      |       |
 |                |      |
    ---------------------

Datas :
E = 12 V
R1 = 1 kΩ
R2 = 2.7 kΩ

Study of the evolution of V2 as a function of R3,
study of R3 power consumption as a function of R3,
and V2 as a function of I3.
""")

# law = dc.Law()

# definitions
E = dc.Voltage(12)
R1 = dc.Resistor(1, 'k')
R2 = dc.Resistor(2.7, 'k')

# R3 list
# listR3 = [0.1, 1, 10, 100, 1000, 10000, 100000]
# listR3 = [10**i for i in range(-1, 6)]
listR3 = [0.1+i*50 for i in range(400)]

listI3 = []
listV2 = []
listP3 = []

print("R3 (Ω)               I3 (A)               V2 (V)               P3 (W)")

for value in listR3:

    R3 = dc.Resistor(value)

    # equivalent resistance
    Req = R1 + (R2//R3)

    # Ohm's law
    I1 = E/Req

    # Ohm's law
    V1 = R1*I1

    # Kirchhoff’s voltage law
    V2 = E-V1

    listV2.append(V2())  # V2() or V2.Value()

    # Ohm's law
    I3 = V2/R3
    listI3.append(I3())

    # R3 power consumption
    P3 = V2*I3
    listP3.append(P3())

    print("{:=20} {:=20} {:=20} {:=20}".format(R3(), I3(), V2(), P3()))

if not import_matplotlib:
    exit("You need to install matplotlib to draw plots")

# matplotlib
fig, axe = plt.subplots()

axe.grid(True)

axe.set_title("V2 = f(R3)", fontsize=16)
axe.set_xlabel("R3 [Ω]")
axe.set_ylabel("V2 [V]")
axe.plot(listR3, listV2, 'b-', linewidth=2.0)

fig2, axe2 = plt.subplots()

axe2.grid(True)

axe2.set_title("P3 = f(R3)", fontsize=16)
axe2.set_xlabel("R3 [Ω]")
axe2.set_ylabel("P3 [W]")

axe2.plot(listR3, listP3, 'r-', linewidth=2.0,
          label="Pmax = {:f} W".format(max(listP3)))
axe2.legend()

fig3, axe3 = plt.subplots()

axe3.grid(True)

axe3.set_title("V2 = f(I3)", fontsize=16)
axe3.set_xlabel("I3 [A]")
axe3.set_ylabel("V2 [V]")

# simple linear regression
fit = np.polyfit(listI3, listV2, 1)

axe3.plot(listI3, listV2, 'g-', linewidth=2.0,
          label="V2 = {:+f}*I3 {:+f}".format(fit[0], fit[1]))
axe3.legend()

plt.show()
