# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

# package dcelectricity example

import dcelectricity.dc_en as dc

print(dc.__version__)
if dc.__version__ < (0, 2, 4):
    raise Exception("package version is too old")

print("""DC Circuit diagram :

          V1
       <------
    --->--R1-------------
 ^     I1         |      |
 |            I2  v      v  I3   ^
 |                |      |       |
E|                R2     R3      | V2
 |                |      |       |
 |                |      |
    ---------------------
""")

"""
Datas :
E = 12 V
R1 = 1 kΩ
R2 = 2.7 kΩ
R3 = 1.8 kΩ
"""

law = dc.Law()

# definitions
E = dc.Voltage(12)
E.Info("E properties :")

R1 = dc.Resistor(1, 'k')
R1.Info("R1 properties :")

R2 = dc.Resistor(2.7, 'k')
R2.Info("R2 properties :")

R3 = dc.Resistor(1.8, 'k')
R3.Info("R3 properties :")

# equivalent resistance
Req = law.Rserie(R1, law.Rparallel(R2, R3))
Req.Info("Req properties :")

# Ohm's law
I1 = law.Ohm(v=E, r=Req)
I1.Info("I1 properties :")

# Ohm's law
V1 = law.Ohm(i=I1, r=R1)
V1.Info("V1 properties :")

# Kirchhoff’s voltage law
V2 = law.KVL("+-", E, V1)
V2.Info("V2 properties :")

# Ohm's law
I2 = law.Ohm(r=R2, v=V2)
I2.Info("I2 properties :")

# Kirchhoff’s current law
I3 = law.KCL("+-", I1, I2)
I3.Info("I3 properties :")

# another way to find V2
# Voltage divider
V2bis = law.VoltageDivider(vtotal=E, r=law.Rparallel(R2, R3), r2=R1)

# another way to find I2
# Current divider
I2bis = law.CurrentDivider(itotal=I1, r=R2, r2=R3)

# yet another way to find V2
# Millman's theorem
gnd = dc.Voltage(0)
V2ter = law.Millman(v_r=[(E, R1), (gnd, R2), (gnd, R3)])

# powers
PE = law.Power(v=E, i=I1)
PE.Info("E power supply :")

P1 = law.Joule(r=R1, i=I1)
P1.Info("Power consumption of R1 :")
P2 = law.Joule(r=R2, i=I2)
P2.Info("Power consumption of R2 :")
P3 = law.Joule(r=R3, i=I3)
P3.Info("Power consumption of R3 :")
(P1+P2+P3).Info("Power consumption of all resistors :")
