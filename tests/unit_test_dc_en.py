# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

import random
import unittest
from dcelectricity import dc_en as dc

# label
e = "Energy"
f = "float"
g = "Conductance"
i = "Current"

p = "Power"
r = "Resistor"
t = "Time"
v = "Voltage"

nc = "nc"  # not to do
typeerr = "TypeError"
diverr = 'ZeroDivisionError'

dict1_obj = dict()
dict1_obj[e] = dc.Energy(random.uniform(1, 1000))
dict1_obj[f] = random.uniform(1, 1000)
dict1_obj[g] = dc.Conductance(random.uniform(1000, 2000))
dict1_obj[i] = dc.Current(random.uniform(1, 1000))
dict1_obj[p] = dc.Power(random.uniform(1, 1000))
dict1_obj[r] = dc.Resistor(random.uniform(1000, 2000))
dict1_obj[t] = dc.Time(random.uniform(1, 1000))
dict1_obj[v] = dc.Voltage(random.uniform(1, 1000))

dict2_obj = dict()
dict2_obj[e] = dc.Energy(random.uniform(1, 1000))
dict2_obj[f] = random.uniform(1, 1000)
dict2_obj[g] = dc.Conductance(random.uniform(1, 1000))
dict2_obj[i] = dc.Current(random.uniform(1, 1000))
dict2_obj[p] = dc.Power(random.uniform(1, 1000))
dict2_obj[r] = dc.Resistor(random.uniform(1, 1000))
dict2_obj[t] = dc.Time(random.uniform(1, 1000))
dict2_obj[v] = dc.Voltage(random.uniform(1, 1000))


class Test(unittest.TestCase):

    def test_EngineeringNotation(self):
        # test fonction EngineeringNotation
        self.assertEqual(dc.EngineeringNotation(999), (999, ''))
        self.assertEqual(dc.EngineeringNotation(1000), (1.0, 'k'))
        self.assertEqual(dc.EngineeringNotation(-1e-6), (-1.0, 'µ'))
        self.assertEqual(dc.EngineeringNotation(0), (0, ''))
        mantisse, prefixe = dc.EngineeringNotation(1200000)
        self.assertEqual("Resistance : {} {}Ω".format(mantisse, prefixe), "Resistance : 1.2 MΩ")

    def test_PrefixNotation(self):
        # test fonction PrefixNotation
        self.assertAlmostEqual(dc.PrefixNotation(20, 'm'), 0.02)
        self.assertAlmostEqual(dc.PrefixNotation(50, 'k'), 50000)
        self.assertAlmostEqual(dc.PrefixNotation(-10000), -10000)

    def test_class_Resistor(self):
        # test class Resistor
        r1 = dc.Resistor(22000)
        r1
        print(r1)
        self.assertIsInstance(r1, dc.Resistor)
        self.assertAlmostEqual(r1.Value(), 22000)

        self.assertEqual(r1.Info("R1 properties :"), "R1 properties :\nResistance : 22000 Ω (22.000000 kΩ)")
        self.assertEqual(r1.Info(), "Resistance : 22000 Ω (22.000000 kΩ)")

        r2 = dc.Resistor(4.7, 'k')
        self.assertIsInstance(r2, dc.Resistor)
        self.assertAlmostEqual(r2.Value(), 4700)

        with self.assertRaises(ValueError):
            r = dc.Resistor(-4.7, 'k')

        r3 = r1+r2
        self.assertIsInstance(r3, dc.Resistor)
        self.assertAlmostEqual(r3.Value(), 26700)

        r3 = r1-r2
        self.assertIsInstance(r3, dc.Resistor)
        self.assertAlmostEqual(r3.Value(), 17300)

        r3 = +r1
        self.assertIsInstance(r3, dc.Resistor)
        self.assertIsNot(r3, r1)
        self.assertAlmostEqual(r3.Value(), 22000)

        r3 = +r1-r2
        self.assertIsInstance(r3, dc.Resistor)
        self.assertAlmostEqual(r3.Value(), 17300)

        r3 = r1//r2
        self.assertIsInstance(r3, dc.Resistor)
        self.assertAlmostEqual(r3.Value(), 3872.659176)

        ra = dc.Resistor(1000)
        rb = dc.Resistor(0)
        rc = ra//rb
        self.assertIsInstance(rc, dc.Resistor)
        self.assertAlmostEqual(rc.Value(), 0)

        ra = dc.Resistor(0)
        rb = dc.Resistor(0)
        rc = ra//rb
        self.assertIsInstance(rc, dc.Resistor)
        self.assertAlmostEqual(rc.Value(), 0)

        i1 = dc.Current(10, 'u')
        v1 = r1*i1
        self.assertIsInstance(v1, dc.Voltage)
        self.assertAlmostEqual(v1.Value(), 0.22)

        r2 = r1*10
        self.assertIsInstance(r2, dc.Resistor)
        self.assertAlmostEqual(r2.Value(), 220000)

        r3 = r2*(r1/(r1+r2))
        self.assertIsInstance(r3, dc.Resistor)
        self.assertAlmostEqual(r3.Value(), 20000)

        r2 = 10*r1
        self.assertIsInstance(r2, dc.Resistor)
        self.assertAlmostEqual(r2.Value(), 220000)

        v1 = i1*r1
        self.assertIsInstance(v1, dc.Voltage)
        self.assertAlmostEqual(v1.Value(), 0.22)

        g1 = 1/r1
        self.assertIsInstance(g1, dc.Conductance)
        self.assertAlmostEqual(g1.Value(), 4.54545e-05)

        r1 = dc.Resistor(22000)
        r2 = dc.Resistor(10000)
        x = r1/r2
        self.assertIsInstance(x, float)
        self.assertAlmostEqual(x, 2.2)

        r3 = r1/10
        self.assertIsInstance(r3, dc.Resistor)
        self.assertAlmostEqual(r3.Value(), 2200)

        g1 = 2/r1
        x = r1*g1
        self.assertIsInstance(x, float)
        self.assertAlmostEqual(x, 2)

    def test_class_Conductance(self):
        # test class Conductance

        G1 = dc.Conductance(0.1)
        self.assertIsInstance(G1, dc.Conductance)
        self.assertAlmostEqual(G1.Value(), 0.1)
        G1
        print(G1)

        G2 = dc.Conductance(4.7, 'm')
        self.assertIsInstance(G2, dc.Conductance)
        self.assertAlmostEqual(G2.Value(), 4.7e-3)

        with self.assertRaises(ValueError):
            G = dc.Conductance(-1, 'm')

        G3 = G1+G2
        self.assertIsInstance(G3, dc.Conductance)
        self.assertAlmostEqual(G3.Value(), 0.1047)

        G3 = G1-G2
        self.assertIsInstance(G3, dc.Conductance)
        self.assertAlmostEqual(G3.Value(), 0.1-4.7e-3)

        G3 = +G1
        self.assertIsInstance(G3, dc.Conductance)
        self.assertIsNot(G3, G1)
        self.assertAlmostEqual(G3.Value(), 0.1)

        G3 = +G1-G2
        self.assertIsInstance(G3, dc.Conductance)
        self.assertAlmostEqual(G3.Value(), 0.1-4.7e-3)

        g1 = dc.Conductance(0.01)
        v1 = dc.Voltage(10)
        i1 = g1*v1
        self.assertIsInstance(i1, dc.Current)
        self.assertAlmostEqual(i1.Value(), 0.1)

        g2 = g1*10
        self.assertIsInstance(g2, dc.Conductance)
        self.assertAlmostEqual(g2.Value(), 0.1)

        g2 = 10*g1
        self.assertIsInstance(g2, dc.Conductance)
        self.assertAlmostEqual(g2.Value(), 0.1)

        g1 = dc.Conductance(0.001)
        r1 = 1/g1
        self.assertIsInstance(r1, dc.Resistor)
        self.assertAlmostEqual(r1.Value(), 1000)

        g1 = dc.Conductance(0.01)
        g2 = dc.Conductance(0.2)
        x = g1/g2  # 0.05
        self.assertIsInstance(x, float)
        self.assertAlmostEqual(x, 0.05)

        g3 = g1/10
        self.assertIsInstance(g3, dc.Conductance)
        self.assertAlmostEqual(g3.Value(), 0.001)

        g3 = g1//g2
        self.assertIsInstance(g3, dc.Conductance)
        self.assertAlmostEqual(g3.Value(), 0.21)

        r1 = 2/g1
        x = g1*r1
        self.assertIsInstance(x, float)
        self.assertAlmostEqual(x, 2)

    def test_class_Law(self):
        # test class Law

        law = dc.Law()
        v1 = dc.Voltage(5)
        v2 = dc.Voltage(-8)
        v3 = dc.Voltage(2.5)

        v4 = law.KVL('++-', v1, v2, v3)
        self.assertIsInstance(v4, dc.Voltage)
        self.assertAlmostEqual(v4.Value(), -5.5)

        i1 = dc.Current(5, 'm')
        i2 = dc.Current(8, 'm')
        i3 = dc.Current(2.5, 'm')

        i4 = law.KCL('-+-', i1, i2, i3)
        self.assertIsInstance(i4, dc.Current)
        self.assertAlmostEqual(i4.Value(), 0.0005)

        v1 = dc.Voltage(5)
        i1 = dc.Current(20, 'µ')
        r1 = law.Ohm(v=v1, i=i1)
        self.assertIsInstance(r1, dc.Resistor)
        self.assertAlmostEqual(r1.Value(), 250000)

        v2 = dc.Voltage(2)
        r2 = dc.Resistor(100, 'k')
        i2 = law.Ohm(v=v2, r=r2)
        self.assertIsInstance(i2, dc.Current)
        self.assertAlmostEqual(i2.Value(), 2e-5)

        i1 = dc.Current(20, 'µ')
        r2 = dc.Resistor(100, 'k')
        v2 = law.Ohm(r=r2, i=i1)
        self.assertIsInstance(v2, dc.Voltage)
        self.assertAlmostEqual(v2.Value(), 2)

        r1 = dc.Resistor(100, 'k')
        r2 = dc.Resistor(47, 'k')
        r3 = dc.Resistor(22, 'k')

        Req = law.Rserie(r1, r2, r3)
        self.assertIsInstance(Req, dc.Resistor)
        self.assertAlmostEqual(Req.Value(), 169000)

        r1 = dc.Resistor(1500)
        r2 = dc.Resistor(1000)

        Req = law.Rparallel(r1, r2)
        self.assertIsInstance(Req, dc.Resistor)
        self.assertAlmostEqual(Req.Value(), 600)

        r1 = dc.Resistor(1, 'k')
        r2 = dc.Resistor(9, 'k')
        v = dc.Voltage(5)

        v1 = law.VoltageDivider(vtotal=v, r=r1, r2=r2)
        self.assertIsInstance(v1, dc.Voltage)
        self.assertAlmostEqual(v1.Value(), 0.5)

        r1 = dc.Resistor(100)
        r2 = dc.Resistor(900)
        i = dc.Current(100, 'm')

        i1 = law.CurrentDivider(itotal=i, r=r1, r2=r2)
        self.assertIsInstance(i1, dc.Current)
        self.assertAlmostEqual(i1.Value(), 0.09)

        masse = dc.Voltage(0)
        E = dc.Voltage(10)
        R1 = dc.Resistor(1000)
        R2 = dc.Resistor(10000)
        R3 = dc.Resistor(2200)

        v2 = law.Millman(v_r=[(E, R1), (masse, R2), (masse, R3)])
        self.assertIsInstance(v2, dc.Voltage)
        self.assertAlmostEqual(v2.Value(), 6.4327485)

        v1 = dc.Voltage(5)
        i1 = dc.Current(100, 'm')
        p1 = law.Power(v=v1, i=i1)
        self.assertIsInstance(p1, dc.Power)
        self.assertAlmostEqual(p1.Value(), 0.5)

        r1 = dc.Resistor(50)
        i1 = dc.Current(100, 'm')
        p1 = law.Joule(r=r1, i=i1)
        self.assertIsInstance(p1, dc.Power)
        self.assertAlmostEqual(p1.Value(), 0.5)

        r2 = dc.Resistor(220)
        v2 = dc.Voltage(5)
        p2 = law.Joule(r=r2, v=v2)
        self.assertIsInstance(p2, dc.Power)
        self.assertAlmostEqual(p2.Value(), 0.1136363636)

        r0 = dc.Resistor(0)
        with self.assertRaises(ValueError):
            p = law.Joule(r0, v=v2)

        gnd = dc.Voltage(0)
        p2 = law.Joule(r0, v=gnd)
        self.assertIsInstance(p2, dc.Power)
        self.assertAlmostEqual(p2.Value(), 0)

    def test_class_Voltage(self):
        # test class Voltage
        v1 = dc.Voltage(0.032)
        v1
        print(v1)
        self.assertIsInstance(v1, dc.Voltage)
        self.assertAlmostEqual(v1.Value(), 0.032)

        v2 = dc.Voltage(100, 'm')
        self.assertIsInstance(v2, dc.Voltage)
        self.assertAlmostEqual(v2.Value(), 0.1)

        v1 = dc.Voltage(5)
        v2 = dc.Voltage(3)
        v3 = v1+v2
        self.assertIsInstance(v3, dc.Voltage)
        self.assertAlmostEqual(v3.Value(), 8)

        v3 = v1-v2
        self.assertIsInstance(v3, dc.Voltage)
        self.assertAlmostEqual(v3.Value(), 2)

        v3 = +v1+v2
        self.assertIsInstance(v3, dc.Voltage)
        self.assertAlmostEqual(v3.Value(), 8)

        v3 = -v2+v1
        self.assertIsInstance(v3, dc.Voltage)
        self.assertAlmostEqual(v3.Value(), 2)

        v1 = dc.Voltage(5)
        r1 = dc.Resistor(1000)
        i1 = v1/r1
        self.assertIsInstance(i1, dc.Current)
        self.assertAlmostEqual(i1.Value(), 5e-3)

        v1 = dc.Voltage(5)
        i1 = dc.Current(5, 'm')
        r1 = v1/i1
        self.assertIsInstance(r1, dc.Resistor)
        self.assertAlmostEqual(r1.Value(), 1000)

        v2 = v1/10
        self.assertIsInstance(v2, dc.Voltage)
        self.assertAlmostEqual(v2.Value(), 0.5)

        x = v1/v2
        self.assertIsInstance(x, float)
        self.assertAlmostEqual(x, 10)

        g1 = dc.Conductance(0.001)
        i1 = v1*g1
        self.assertIsInstance(i1, dc.Current)
        self.assertAlmostEqual(i1.Value(), 5e-3)

        p1 = v1*i1
        self.assertIsInstance(p1, dc.Power)
        self.assertAlmostEqual(p1.Value(), 25e-3)

        v2 = v1*0.1
        self.assertIsInstance(v2, dc.Voltage)
        self.assertAlmostEqual(v2.Value(), 0.5)

        v2 = 0.1*v1
        self.assertIsInstance(v2, dc.Voltage)
        self.assertAlmostEqual(v2.Value(), 0.5)

        gnd = dc.Voltage(0)
        with self.assertRaises(ZeroDivisionError):
            x = v1/gnd

        i0 = dc.Current(0)
        with self.assertRaises(ZeroDivisionError):
            r = v1/i0

        with self.assertRaises(ZeroDivisionError):
            v = v1/0

        r0 = dc.Resistor(0)
        with self.assertRaises(ZeroDivisionError):
            i = v1/r0

    def test_class_Current(self):
        # test class Current

        i1 = dc.Current(32, 'm')
        self.assertIsInstance(i1, dc.Current)
        self.assertAlmostEqual(i1.Value(), 32e-3)
        i1
        print(i1)

        i2 = dc.Current(0.050)
        i3 = i1+i2
        self.assertIsInstance(i3, dc.Current)
        self.assertAlmostEqual(i3.Value(), 82e-3)

        i3 = i1-i2
        self.assertIsInstance(i3, dc.Current)
        self.assertAlmostEqual(i3.Value(), -18e-3)

        i3 = +i1+i2
        self.assertIsInstance(i3, dc.Current)
        self.assertAlmostEqual(i3.Value(), 82e-3)

        i3 = -i2+i1
        self.assertIsInstance(i3, dc.Current)
        self.assertAlmostEqual(i3.Value(), -18e-3)

        i2 = i1/10
        self.assertIsInstance(i2, dc.Current)
        self.assertAlmostEqual(i2.Value(), 3.2e-3)

        x = i1/i2
        self.assertIsInstance(x, float)
        self.assertAlmostEqual(x, 10)

        i2 = i1*0.1
        self.assertIsInstance(i2, dc.Current)
        self.assertAlmostEqual(i2.Value(), 3.2e-3)

        i2 = 0.1*i1
        self.assertIsInstance(i2, dc.Current)
        self.assertAlmostEqual(i2.Value(), 3.2e-3)

        v1 = dc.Voltage(10)
        i1 = dc.Current(0.01)
        g1 = i1/v1
        self.assertIsInstance(g1, dc.Conductance)
        self.assertAlmostEqual(g1.Value(), 0.001)

        gnd = dc.Voltage(0)
        with self.assertRaises(ZeroDivisionError):
            g = i1/gnd

        i0 = dc.Current(0)
        with self.assertRaises(ZeroDivisionError):
            x = i1/i0

        with self.assertRaises(ZeroDivisionError):
            i = i1/0

        g0 = dc.Conductance(0)
        with self.assertRaises(ZeroDivisionError):
            v = i1/g0

    def test_class_Power(self):
        # test class Power
        p1 = dc.Power(10, "M")
        p1
        print(p1)
        self.assertIsInstance(p1, dc.Power)
        self.assertAlmostEqual(p1.Value(), 10e6)

        p2 = dc.Power(2.5, "M")
        p3 = p1+p2
        self.assertIsInstance(p3, dc.Power)
        self.assertAlmostEqual(p3.Value(), 12.5e6)

        p3 = +p1+p2
        self.assertIsInstance(p3, dc.Power)
        self.assertAlmostEqual(p3.Value(), 12.5e6)

        p3 = p1-p2
        self.assertIsInstance(p3, dc.Power)
        self.assertAlmostEqual(p3.Value(), 7.5e6)

        p3 = -p2+p1
        self.assertIsInstance(p3, dc.Power)
        self.assertAlmostEqual(p3.Value(), 7.5e6)

        p1 = dc.Power(0.05)
        v1 = dc.Voltage(10)
        i1 = p1/v1
        self.assertIsInstance(i1, dc.Current)
        self.assertAlmostEqual(i1.Value(), 5e-3)

        p2 = p1/5
        self.assertIsInstance(p2, dc.Power)
        self.assertAlmostEqual(p2.Value(), 0.01)

        v2 = p2/i1
        self.assertIsInstance(v2, dc.Voltage)
        self.assertAlmostEqual(v2.Value(), 2)

        x = p1/p2
        self.assertIsInstance(x, float)
        self.assertAlmostEqual(x, 5)

        p2 = p1*10
        self.assertIsInstance(p2, dc.Power)
        self.assertAlmostEqual(p2.Value(), 0.5)

        p2 = 10.0*p1
        self.assertIsInstance(p2, dc.Power)
        self.assertAlmostEqual(p2.Value(), 0.5)

        e1 = dc.Energy(1000)
        t1 = dc.Time(100)
        p1 = e1/t1
        self.assertIsInstance(p1, dc.Power)
        self.assertAlmostEqual(p1.Value(), 10)

    def test_getenergy(self):
        self.assertAlmostEqual(dc.getenergy(1000, 'J'), 1000)
        self.assertAlmostEqual(dc.getenergy(200, 'kJ'), 200000)
        self.assertAlmostEqual(dc.getenergy(2, 'kWh'), 7.2e6)

    def test_gettime(self):
        self.assertAlmostEqual(dc.gettime(1000, 's'), 1000)
        self.assertAlmostEqual(dc.gettime(200, 'ms'), 0.2)
        self.assertAlmostEqual(dc.gettime(0.5, 'h'), 1800)

    def test_class_Time(self):
        # test class Time

        t1 = dc.Time(1800)
        t1
        print(t1)
        self.assertIsInstance(t1, dc.Time)
        self.assertAlmostEqual(t1(), 1800)

        t1 = dc.Time(1800, 's')
        self.assertIsInstance(t1, dc.Time)
        self.assertAlmostEqual(t1(), 1800)

        t1 = dc.Time(3600, 'ms')
        self.assertIsInstance(t1, dc.Time)
        self.assertAlmostEqual(t1(), 3.6)
        self.assertAlmostEqual(t1('s'), 3.6)
        self.assertAlmostEqual(t1('h'), 0.001)
        with self.assertRaises(ValueError):
            t1('p')

        t1 = dc.Time(3, 'h')
        self.assertIsInstance(t1, dc.Time)
        self.assertAlmostEqual(t1(), 10800)

        t2 = dc.Time(7200)
        t3 = t1+t2
        self.assertIsInstance(t3, dc.Time)
        self.assertAlmostEqual(t3(), 18000)

        t3 = +t1+t2
        self.assertIsInstance(t3, dc.Time)
        self.assertAlmostEqual(t3(), 18000)

        t3 = t1-t2
        self.assertIsInstance(t3, dc.Time)
        self.assertAlmostEqual(t3(), 3600)

        t3 = -t2+t1
        self.assertIsInstance(t3, dc.Time)
        self.assertAlmostEqual(t3(), 3600)

        t3 = 10*t1/10
        self.assertIsInstance(t3, dc.Time)
        self.assertAlmostEqual(t3(), 10800)

        p1 = dc.Power(1000)
        e1 = dc.Energy(10)
        t3 = e1/p1
        self.assertIsInstance(t3, dc.Time)
        self.assertAlmostEqual(t3(), 0.01)

        t2 = t1/10
        self.assertIsInstance(t2, dc.Time)
        self.assertAlmostEqual(t2(), 1080)

        x = t2/t1
        self.assertIsInstance(x, float)
        self.assertAlmostEqual(x, 0.1)

        t2 = t1*10
        self.assertIsInstance(t2, dc.Time)
        self.assertAlmostEqual(t2(), 108000)

        t2 = 10.0*t1
        self.assertIsInstance(t2, dc.Time)
        self.assertAlmostEqual(t2(), 108000)

    def test_class_Energy(self):
        # test class Energy
        e1 = dc.Energy(1.8, "MJ")
        e1
        print(e1)

        self.assertIsInstance(e1, dc.Energy)
        self.assertAlmostEqual(e1.Value(), 1.8e6)
        self.assertAlmostEqual(e1(), 1.8e6)
        self.assertAlmostEqual(e1('J'), 1.8e6)
        self.assertAlmostEqual(e1('kWh'), 0.5)
        with self.assertRaises(ValueError):
            e1('wh')

        e2 = dc.Energy(2, "kWh")
        e3 = e1+e2
        self.assertIsInstance(e3, dc.Energy)
        self.assertAlmostEqual(e3.Value(), 9e6)

        e3 = +e1+e2
        self.assertIsInstance(e3, dc.Energy)
        self.assertAlmostEqual(e3.Value(), 9e6)

        e3 = e1-e2
        self.assertIsInstance(e3, dc.Energy)
        self.assertAlmostEqual(e3.Value(), -5.4e6)

        e3 = -e2+e1
        self.assertIsInstance(e3, dc.Energy)
        self.assertAlmostEqual(e3.Value(), -5.4e6)

        e3 = 10*e1/10
        self.assertIsInstance(e3, dc.Energy)
        self.assertAlmostEqual(e3.Value(), 1.8e6)

        p1 = dc.Power(0.05)
        t1 = dc.Time(10)
        e3 = p1*t1
        self.assertIsInstance(e3, dc.Energy)
        self.assertAlmostEqual(e3.Value(), 0.5)

        e2 = e1/10
        self.assertIsInstance(e2, dc.Energy)
        self.assertAlmostEqual(e2.Value(), 0.18e6)

        x = e2/e1
        self.assertIsInstance(x, float)
        self.assertAlmostEqual(x, 0.1)

        e2 = e1*10
        self.assertIsInstance(e2, dc.Energy)
        self.assertAlmostEqual(e2.Value(), 18e6)

        e2 = 10.0*e1
        self.assertIsInstance(e2, dc.Energy)
        self.assertAlmostEqual(e2.Value(), 18e6)

    def test_mul(self):
        # multiplication
        matrice = dict()  # unit matrix

        # label
        e = "Energy"
        f = "float"
        g = "Conductance"
        i = "Current"

        p = "Power"
        r = "Resistor"
        t = "Time"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # e, f, g , i, p, r, t, v
        matrice[e, e], matrice[e, f], matrice[e, g], matrice[e, i], matrice[e, p], matrice[e, r], matrice[e, t], matrice[e, v] = typeerr, e, typeerr, typeerr, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[f, e], matrice[f, f], matrice[f, g], matrice[f, i], matrice[f, p], matrice[f, r], matrice[f, t], matrice[f, v] = e, f, g, i, p, r, t, v

        # e, f, g , i, p, r, t, v
        matrice[g, e], matrice[g, f], matrice[g, g], matrice[g, i], matrice[g, p], matrice[g, r], matrice[g, t], matrice[g, v] = typeerr, g, typeerr, typeerr, typeerr, f, typeerr, i

        # e, f, g , i, p, r, t, v
        matrice[i, e], matrice[i, f], matrice[i, g], matrice[i, i], matrice[i, p], matrice[i, r], matrice[i, t], matrice[i, v] = typeerr, i, typeerr, typeerr, typeerr, v, typeerr, p

        # e, f, g , i, p, r, t, v
        matrice[p, e], matrice[p, f], matrice[p, g], matrice[p, i], matrice[p, p], matrice[p, r], matrice[p, t], matrice[p, v] = typeerr, p, typeerr, typeerr, typeerr, typeerr, e, typeerr

        # e, f, g , i, p, r, t, v
        matrice[r, e], matrice[r, f], matrice[r, g], matrice[r, i], matrice[r, p], matrice[r, r], matrice[r, t], matrice[r, v] = typeerr, r, f, v, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[t, e], matrice[t, f], matrice[t, g], matrice[t, i], matrice[t, p], matrice[t, r], matrice[t, t], matrice[t, v] = typeerr, t, typeerr, typeerr, e, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[v, e], matrice[v, f], matrice[v, g], matrice[v, i], matrice[v, p], matrice[v, r], matrice[v, t], matrice[v, v] = typeerr, v, i, p, typeerr, typeerr, typeerr, typeerr

        for i in matrice:
            arg1, arg2 = i  # 'Energy', 'Current'
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]   # dc.Energy(11.3)
            obj2 = dict2_obj[arg2]   # dc.Current(0.581)

            # Energy * float -> Energy
            # Energy * Conductance -> TypeError
            print(arg1, '*', arg2, '->',  matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1*obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1*obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1*obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float)):
                    # not callable
                    val1 = obj1
                else:
                    val1 = obj1()
                if isinstance(obj2, (int, float)):
                    val2 = obj2
                else:
                    val2 = obj2()
                val = val1*val2  # expected value

                if isinstance(obj, (int, float)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(), val)

    def test_rmul(self):
        # right multiplication
        matrice = dict()  # unit matrix

        # label
        e = "Energy"
        f = "float"
        g = "Conductance"
        i = "Current"

        p = "Power"
        r = "Resistor"
        t = "Time"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # e, f, g , i, p, r, t, v
        matrice[f, e], matrice[f, g], matrice[f, i], matrice[f, p], matrice[f, r], matrice[f, t], matrice[f, v] = e, g, i, p, r, t, v

        for i in matrice:
            arg1, arg2 = i  # 'Energy', 'Current'
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]   # dc.Energy(11.3)
            obj2 = dict2_obj[arg2]   # dc.Current(0.581)

            print(arg1, '* ', arg2, '->',  matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1*obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1*obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1*obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float)):
                    # not callable
                    val1 = obj1
                else:
                    val1 = obj1()
                if isinstance(obj2, (int, float)):
                    val2 = obj2
                else:
                    val2 = obj2()
                val = val1*val2  # expected value

                if isinstance(obj, (int, float)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(), val)

    def test_add(self):
        # addition
        matrice = dict()  # unit matrix

        # label
        e = "Energy"
        f = "float"
        g = "Conductance"
        i = "Current"

        p = "Power"
        r = "Resistor"
        t = "Time"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # e, f, g , i, p, r, t, v
        matrice[e, e], matrice[e, f], matrice[e, g], matrice[e, i], matrice[e, p], matrice[e, r], matrice[e, t], matrice[e, v] = e, typeerr, typeerr, typeerr, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[f, e], matrice[f, f], matrice[f, g], matrice[f, i], matrice[f, p], matrice[f, r], matrice[f, t], matrice[f, v] = typeerr, f, typeerr, typeerr, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[g, e], matrice[g, f], matrice[g, g], matrice[g, i], matrice[g, p], matrice[g, r], matrice[g, t], matrice[g, v] = typeerr, typeerr, g, typeerr, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[i, e], matrice[i, f], matrice[i, g], matrice[i, i], matrice[i, p], matrice[i, r], matrice[i, t], matrice[i, v] = typeerr, typeerr, typeerr, i, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[p, e], matrice[p, f], matrice[p, g], matrice[p, i], matrice[p, p], matrice[p, r], matrice[p, t], matrice[p, v] = typeerr, typeerr, typeerr, typeerr, p, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[r, e], matrice[r, f], matrice[r, g], matrice[r, i], matrice[r, p], matrice[r, r], matrice[r, t], matrice[r, v] = typeerr, typeerr, typeerr, typeerr,  typeerr, r, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[t, e], matrice[t, f], matrice[t, g], matrice[t, i], matrice[t, p], matrice[t, r], matrice[t, t], matrice[t, v] = typeerr, typeerr, typeerr, typeerr, typeerr, typeerr, t, typeerr

        # e, f, g , i, p, r, t, v
        matrice[v, e], matrice[v, f], matrice[v, g], matrice[v, i], matrice[v, p], matrice[v, r], matrice[v, t], matrice[v, v] = typeerr, typeerr, typeerr, typeerr, typeerr, typeerr, typeerr, v

        for i in matrice:
            arg1, arg2 = i  # 'Energy', 'Current'
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]   # dc.Energy(11.3)
            obj2 = dict2_obj[arg2]   # dc.Current(0.581)

            # Energy * float -> Energy
            # Energy * Conductance -> TypeError
            print(arg1, '+', arg2, '->',  matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1+obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1+obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1+obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float)):
                    # not callable
                    val1 = obj1
                else:
                    val1 = obj1()
                if isinstance(obj2, (int, float)):
                    val2 = obj2
                else:
                    val2 = obj2()
                val = val1+val2  # expected value

                if isinstance(obj, (int, float)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(), val)

    def test_sub(self):
        # substraction
        matrice = dict()  # unit matrix

        # label
        e = "Energy"
        f = "float"
        g = "Conductance"
        i = "Current"

        p = "Power"
        r = "Resistor"
        t = "Time"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # e, f, g , i, p, r, t, v
        matrice[e, e], matrice[e, f], matrice[e, g], matrice[e, i], matrice[e, p], matrice[e, r], matrice[e, t], matrice[e, v] = e, typeerr, typeerr, typeerr, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[f, e], matrice[f, f], matrice[f, g], matrice[f, i], matrice[f, p], matrice[f, r], matrice[f, t], matrice[f, v] = typeerr, f, typeerr, typeerr, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[g, e], matrice[g, f], matrice[g, g], matrice[g, i], matrice[g, p], matrice[g, r], matrice[g, t], matrice[g, v] = typeerr, typeerr, g, typeerr, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[i, e], matrice[i, f], matrice[i, g], matrice[i, i], matrice[i, p], matrice[i, r], matrice[i, t], matrice[i, v] = typeerr, typeerr, typeerr, i, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[p, e], matrice[p, f], matrice[p, g], matrice[p, i], matrice[p, p], matrice[p, r], matrice[p, t], matrice[p, v] = typeerr, typeerr, typeerr, typeerr, p, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[r, e], matrice[r, f], matrice[r, g], matrice[r, i], matrice[r, p], matrice[r, r], matrice[r, t], matrice[r, v] = typeerr, typeerr, typeerr, typeerr,  typeerr, r, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[t, e], matrice[t, f], matrice[t, g], matrice[t, i], matrice[t, p], matrice[t, r], matrice[t, t], matrice[t, v] = typeerr, typeerr, typeerr, typeerr, typeerr, typeerr, t, typeerr

        # e, f, g , i, p, r, t, v
        matrice[v, e], matrice[v, f], matrice[v, g], matrice[v, i], matrice[v, p], matrice[v, r], matrice[v, t], matrice[v, v] = typeerr, typeerr, typeerr, typeerr, typeerr, typeerr, typeerr, v

        for i in matrice:
            arg1, arg2 = i  # 'Energy', 'Current'
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]   # dc.Energy(11.3)
            obj2 = dict2_obj[arg2]   # dc.Current(0.581)

            # Energy * float -> Energy
            # Energy * Conductance -> TypeError
            print(arg1, '-', arg2, '->',  matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1-obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1-obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1-obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float)):
                    # not callable
                    val1 = obj1
                else:
                    val1 = obj1()
                if isinstance(obj2, (int, float)):
                    val2 = obj2
                else:
                    val2 = obj2()
                val = val1-val2  # expected value

                if isinstance(obj, (int, float)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(), val)

    def test_pos(self):
        # +self
        matrice = dict()  # unit matrix

        # label
        e = "Energy"
        f = "float"
        g = "Conductance"
        i = "Current"

        p = "Power"
        r = "Resistor"
        t = "Time"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # e, f, g , i, p, r, t, v
        matrice[e], matrice[f], matrice[g], matrice[i], matrice[p], matrice[r], matrice[t], matrice[v] = e, f, g, i, p, r, t, v

        for i in matrice:
            arg1 = i  # 'Energy'
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]   # dc.Energy(11.3)

            # +Energy -> Energy

            print('+', arg1,  '->',  matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    +obj1
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    +obj1
            elif matrice[i] == nc:
                pass
            else:
                obj = +obj1
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float)):
                    # not callable
                    val1 = obj1
                else:
                    val1 = obj1()

                val = val1  # expected value

                if isinstance(obj, (int, float)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(), val)

    def test_neg(self):
        # -self
        matrice = dict()  # unit matrix

        # label
        e = "Energy"
        f = "float"
        g = "Conductance"
        i = "Current"

        p = "Power"
        r = "Resistor"
        t = "Time"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # e, f, g , i, p, r, t, v
        matrice[e], matrice[f], matrice[g], matrice[i], matrice[p], matrice[r], matrice[t], matrice[v] = e, f, typeerr, i, p, typeerr, t, v

        for i in matrice:
            arg1 = i  # 'Energy'
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]   # dc.Energy(11.3)

            # +Energy -> Energy

            print('-', arg1,  '->',  matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    -obj1
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    -obj1
            elif matrice[i] == nc:
                pass
            else:
                obj = -obj1
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float)):
                    # not callable
                    val1 = obj1
                else:
                    val1 = obj1()

                val = -val1  # expected value

                if isinstance(obj, (int, float)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(), val)

    def test_rtruediv(self):
        # right division
        matrice = dict()  # unit matrix

        # label
        e = "Energy"
        f = "float"
        g = "Conductance"
        i = "Current"

        p = "Power"
        r = "Resistor"
        t = "Time"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # e, f, g , i, p, r, t, v
        matrice[f, e], matrice[f, g], matrice[f, i], matrice[f, p], matrice[f, r], matrice[f, t], matrice[f, v] = typeerr, r, typeerr, typeerr, g, typeerr, typeerr

        for i in matrice:
            arg1, arg2 = i  # 'Energy', 'Current'
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]   # dc.Energy(11.3)
            obj2 = dict2_obj[arg2]   # dc.Current(0.581)

            print(arg1, '/ ', arg2, '->',  matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1/obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1/obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1/obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float)):
                    # not callable
                    val1 = obj1
                else:
                    val1 = obj1()
                if isinstance(obj2, (int, float)):
                    val2 = obj2
                else:
                    val2 = obj2()
                val = val1/val2  # expected value

                if isinstance(obj, (int, float)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(), val)

    def test_truediv(self):
        # division
        matrice = dict()  # unit matrix

        # label
        e = "Energy"
        f = "float"
        g = "Conductance"
        i = "Current"

        p = "Power"
        r = "Resistor"
        t = "Time"
        v = "Voltage"

        nc = "nc"  # not to do
        typeerr = "TypeError"
        diverr = 'ZeroDivisionError'

        # e, f, g , i, p, r, t, v
        matrice[e, e], matrice[e, f], matrice[e, g], matrice[e, i], matrice[e, p], matrice[e, r], matrice[e, t], matrice[e, v] = f, e, typeerr, typeerr, t, typeerr, p, typeerr

        # e, f, g , i, p, r, t, v
        matrice[f, e], matrice[f, f], matrice[f, g], matrice[f, i], matrice[f, p], matrice[f, r], matrice[f, t], matrice[f, v] = typeerr, f, r, typeerr, typeerr, g, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[g, e], matrice[g, f], matrice[g, g], matrice[g, i], matrice[g, p], matrice[g, r], matrice[g, t], matrice[g, v] = typeerr, g, f, typeerr, typeerr, typeerr, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[i, e], matrice[i, f], matrice[i, g], matrice[i, i], matrice[i, p], matrice[i, r], matrice[i, t], matrice[i, v] = typeerr, i, v, f, typeerr, typeerr, typeerr, g

        # e, f, g , i, p, r, t, v
        matrice[p, e], matrice[p, f], matrice[p, g], matrice[p, i], matrice[p, p], matrice[p, r], matrice[p, t], matrice[p, v] = typeerr, p, typeerr, v, f, typeerr, typeerr, i

        # e, f, g , i, p, r, t, v
        matrice[r, e], matrice[r, f], matrice[r, g], matrice[r, i], matrice[r, p], matrice[r, r], matrice[r, t], matrice[r, v] = typeerr, r, typeerr, typeerr, typeerr, f, typeerr, typeerr

        # e, f, g , i, p, r, t, v
        matrice[t, e], matrice[t, f], matrice[t, g], matrice[t, i], matrice[t, p], matrice[t, r], matrice[t, t], matrice[t, v] = typeerr, t, typeerr, typeerr, typeerr, typeerr, f, typeerr

        # e, f, g , i, p, r, t, v
        matrice[v, e], matrice[v, f], matrice[v, g], matrice[v, i], matrice[v, p], matrice[v, r], matrice[v, t], matrice[v, v] = typeerr, v, typeerr, r, typeerr, i, typeerr, f

        for i in matrice:
            arg1, arg2 = i  # 'Energy', 'Current'
            # print(arg1, arg2)

            obj1 = dict1_obj[arg1]   # dc.Energy(11.3)
            obj2 = dict2_obj[arg2]   # dc.Current(0.581)

            # Energy * float -> Energy
            # Energy * Conductance -> TypeError
            print(arg1, '/', arg2, '->',  matrice[i])

            if matrice[i] == typeerr:
                with self.assertRaises(TypeError):
                    obj1/obj2
            elif matrice[i] == diverr:
                with self.assertRaises(ZeroDivisionError):
                    obj1/obj2
            elif matrice[i] == nc:
                pass
            else:
                obj = obj1/obj2
                self.assertIsInstance(obj, dict1_obj[matrice[i]].__class__)
                if isinstance(obj1, (int, float)):
                    # not callable
                    val1 = obj1
                else:
                    val1 = obj1()
                if isinstance(obj2, (int, float)):
                    val2 = obj2
                else:
                    val2 = obj2()
                val = val1/val2  # expected value

                if isinstance(obj, (int, float)):
                    self.assertAlmostEqual(obj, val)
                else:
                    self.assertAlmostEqual(obj(), val)


if __name__ == '__main__':
    unittest.main()
